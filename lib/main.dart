import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

import 'package:Vanecontrol/generated/i18n.dart';
import 'package:Vanecontrol/src/pages/qrcode/introduce.dart';

void main() {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static var _primaryColor = const Color(0XFF0097A9);
  static var _titleStyle = TextStyle(
      fontSize: 35, fontFamily: 'HelveticaNeue_Light', color: Colors.white);
  static var _labelStyle = TextStyle(
      fontSize: 18,
      fontFamily: 'HelveticaNeue_Roman',
      color: Colors.white);

  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? MaterialApp(
            localizationsDelegates: [S.delegate],
            supportedLocales: S.delegate.supportedLocales,
            localeResolutionCallback:
                S.delegate.resolution(fallback: new Locale('en', '')),
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                primarySwatch: Colors.blue,
                primaryColor: _primaryColor,
                accentColor: Color(0XFFFFAE00),
                textTheme: TextTheme(
                    title: _titleStyle,
                    headline: _labelStyle
                )),
            home: QRCodeIntroduceWidget())
        : CupertinoApp(
            localizationsDelegates: [S.delegate],
            supportedLocales: S.delegate.supportedLocales,
            localeResolutionCallback:
                S.delegate.resolution(fallback: new Locale('en', '')),
            debugShowCheckedModeBanner: false,
            theme: CupertinoThemeData(
                primaryColor: _primaryColor,
                primaryContrastingColor: Color(0XFFFFAE00),
                textTheme: CupertinoTextThemeData(
                    navLargeTitleTextStyle: _titleStyle,
                    tabLabelTextStyle: _labelStyle
                )
            ),
            home: QRCodeIntroduceWidget(),
          );
  }
}
