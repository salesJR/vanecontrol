import 'package:flutter/material.dart';

class MyCheckbox extends StatefulWidget {
  bool sensor;

  MyCheckbox(this.sensor);

  @override
  State<StatefulWidget> createState() => _MyCheckboxState();
}

class _MyCheckboxState extends State<MyCheckbox> {
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          color: Colors.white,
          width: 25,
          height: 25,
          child: Container(
              decoration: BoxDecoration(
                  border: Border.all(
                      width: 3,
                      color: widget.sensor
                          ? Theme.of(context).primaryColor
                          : Theme.of(context).accentColor),
                  borderRadius: BorderRadius.all(Radius.circular(5))),
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    _isChecked = !_isChecked;
                  });
                },
                child: Padding(
                  padding: EdgeInsets.all(1),
                  child: Container(
                    decoration: BoxDecoration(
                        color: (_isChecked)
                            ? widget.sensor
                                ? Theme.of(context).primaryColor
                                : Theme.of(context).accentColor
                            : Colors.white,
                        border: Border.all(
                          width: 3,
                          color: (_isChecked)
                              ? widget.sensor
                                  ? Theme.of(context).primaryColor
                                  : Theme.of(context).accentColor
                              : Colors.white,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(3))),
                  ),
                ),
              )),
        )
      ],
    ));
  }
}
