import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ToggleFieldWidget extends StatefulWidget {

  final TextEditingController controller;

  ToggleFieldWidget({this.controller});

  @override
  State<StatefulWidget> createState() => _ToggleFieldWidgetState();
}

class _ToggleFieldWidgetState extends State<ToggleFieldWidget> {
  bool _obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, top: 10),
      child: TextFormField(
        controller: widget.controller,
        obscureText: _obscureText,
        decoration: InputDecoration(
            border: OutlineInputBorder(borderSide: BorderSide.none),
            fillColor: Colors.white,
            filled: true,
            hintText: 'Senha',
            prefixIcon:
                IconButton(icon: SvgPicture.asset('assets/icons/padlock.svg')),
            suffixIcon: IconButton(
              icon: SvgPicture.asset(
                'assets/icons/eye.svg',
                width: 10,
                height: 10,
              ),
              onPressed: () {
                setState(() {
                  _obscureText = !_obscureText;
                });
              },
            )),
        validator: (value) {
          if (value.isEmpty) {
            return 'Please enter some text';
          }
        },
      ),
    );
  }
}
