class Device {

  final String _name;
  final String _type;

  Device(this._name, this._type);

  String get name {
    return this._name;
  }

  String get type {
    return this._type;
  }
}