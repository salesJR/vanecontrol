import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:Vanecontrol/generated/i18n.dart';
import 'package:Vanecontrol/src/model/device.dart';

class ProfileWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileState();
}

class _ProfileState extends State<ProfileWidget> {
  final List<Device> _devices = [
    Device("Vane 01234", "Sensor"),
    Device("Vane 01234", "Aleta"),
    Device("Vane 01234", "Aleta"),
    Device("Vane 01234", "Aleta")
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).primaryColor,
      child: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 30),
            child: SvgPicture.asset('assets/icons/devices.svg'),
          ),
          Container(
              margin: EdgeInsets.only(top: 40),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 100),
                child: Column(
                  children: <Widget>[
                    Text(S.of(context).profile_subhead,
                        style: TextStyle(
                            fontFamily: 'HelveticaNeue_Roman',
                            fontSize: 16,
                            color: Colors.white)),
                    Container(
                      margin: EdgeInsets.zero,
                      child: TextField(
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white)),
                            suffixIcon: SizedBox(
                              width: 1,
                              height: 2,
                              child: IconButton(
                                padding: EdgeInsetsDirectional.zero,
                                icon: SvgPicture.asset(
                                    'assets/icons/change_white.svg'),
                              ),
                            )),
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'HelveticaNeue_Bold'),
                      ),
                    ),
                  ],
                ),
              )),
          Padding(
              padding: EdgeInsets.all(25),
              child: SizedBox(
                width: double.infinity,
                height: 250,
                child: Card(
                  elevation: 4,
                  child: ListView.builder(
                      itemCount: _devices.length,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTile(
                          leading: SizedBox(
                            height: double.infinity,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                _devices[index].type == "Sensor"
                                    ? SvgPicture.asset(
                                        'assets/icons/sensor_alone.svg')
                                    : SvgPicture.asset(
                                        'assets/icons/aleta_alone.svg'),
                              ],
                            ),
                          ),
                          title: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                _devices[index].name,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'HelveticaNeue_Roman',
                                    color: Colors.black),
                              ),
                              Text(
                                _devices[index].type,
                                style: TextStyle(
                                    fontSize: 16,
                                    fontFamily: 'HelveticaNeue_Roman',
                                    color: Colors.black.withOpacity(.5)),
                              ),
                            ],
                          ),
                          trailing: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              GestureDetector(
                                child:
                                    SvgPicture.asset('assets/icons/trash.svg'),
                                onTap: () {
                                  setState(() {
                                    _devices.remove(_devices[index]);
                                  });
                                },
                              )
                            ],
                          ),
                        );
                      }),
                ),
              )),
          SizedBox(
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    width: double.infinity,
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 50),
                        child: RaisedButton(
                            onPressed: () {},
                            color: Theme.of(context).accentColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50.0)),
                            child: Text(
                              S.of(context).btn_save_profile.toUpperCase(),
                              style: TextStyle(color: Colors.white),
                            ))),
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(horizontal: 50),
                      child: FlatButton(
                          onPressed: () {},
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0)),
                          child: Text(
                            S.of(context).wifi_btn_cancel,
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'HelveticaNeue_Roman',
                                color: Colors.white),
                          ))),
                ],
              ))
        ],
      )),
    );
  }
}
