import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:Vanecontrol/generated/i18n.dart';
import 'package:Vanecontrol/src/widget/sliding.dart';
import 'package:Vanecontrol/src/widget/checkbox.dart';

class DevicesWidget extends StatelessWidget {
  final double _panelHeightOpen = 380.0;
  final double _panelHeightClosed = 95.0;

  final List<String> networks = [
    "Minha Casa",
    "Vizinho 01",
    "Vizinho 02",
    "Vizinho 03",
    "Vizinho 04",
    "Vizinho 05"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
          child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20),
                  child: SvgPicture.asset('assets/icons/devices.svg'),
                ),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                          text: S.of(context).next_steps_header,
                          style: Theme.of(context).textTheme.title),
                      TextSpan(
                          text: S.of(context).next_steps_subhead,
                          style: Theme.of(context).textTheme.headline)
                    ],
                  ),
                )
              ],
            ),
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: SlidingUpPanel(
                renderPanelSheet: false,
                maxHeight: _panelHeightOpen,
                minHeight: _panelHeightClosed,
                panel: Container(
                    color: Theme.of(context).primaryColor,
                    child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10.0),
                                topRight: Radius.circular(10.0))),
                        child: Stack(
                          overflow: Overflow.visible,
                          alignment: Alignment.topCenter,
                          children: <Widget>[
                            Positioned(
                              top: -15,
                              width: 50,
                              height: 5,
                              child: Container(
                                width: 50,
                                height: 5,
                                decoration: BoxDecoration(
                                    color: Colors.grey[300],
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(12.0))),
                              ),
                            ),
                            Positioned(
                              top: -30,
                              right: 30,
                              width: 70,
                              height: 70,
                              child: FloatingActionButton(
                                backgroundColor: Theme.of(context).accentColor,
                                onPressed: () {
                                },
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                SizedBox(
                                  height: 30,
                                ),
                                Expanded(
                                  child: ListView.builder(
                                      itemCount: networks.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return ListTile(
                                            contentPadding:
                                                EdgeInsets.all(10.0),
                                            leading: index == 0
                                                ? SvgPicture.asset(
                                                    'assets/icons/sensor.svg')
                                                : SvgPicture.asset(
                                                    'assets/icons/aleta.svg'),
                                            title: Text(networks[index]),
                                            trailing: MyCheckbox(index == 0));
                                      }),
                                )
                              ],
                            )
                          ],
                        ))),
              ))
        ],
      )),
    );
  }
}
