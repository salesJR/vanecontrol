import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';

class WelcomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Theme.of(context).primaryColor,
        child: SafeArea(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.topCenter,
                margin: EdgeInsets.only(top: 10),
                child: SvgPicture.asset('assets/icons/welcome_background.svg'),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: 160,
                    height: 160,
                    margin: EdgeInsets.only(bottom: 40),
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: ExactAssetImage('assets/images/girl.png'),
                            fit: BoxFit.cover),
                        borderRadius: BorderRadius.all(Radius.circular(160)),
                        border: Border.all(
                            width: 4, color: Theme.of(context).accentColor)),
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      style: TextStyle(
                        fontFamily: 'Monoton',
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            text: 'BEM - VINDO\n',
                            style: TextStyle(
                              fontFamily: 'Chenla',
                              fontSize: 31,
                            )),
                        TextSpan(
                            text: 'Fulana de Tal dos Anzois',
                            style: TextStyle(
                              fontSize: 16,
                            ))
                      ],
                    ),
                  )
                ],
              ),
              Container(
                alignment: Alignment.bottomCenter,
                margin: EdgeInsets.only(bottom: 40),
                child: RichText(
                  text: TextSpan(
                      text: 'Estamos iniciando o app',
                      style: TextStyle(fontSize: 10)),
                ),
              )
            ],
          ),
        ));
  }
}
