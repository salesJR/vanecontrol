import 'package:Vanecontrol/generated/i18n.dart';
import 'package:Vanecontrol/src/pages/wifi/connection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:Vanecontrol/src/widget/sliding.dart';

class WifiWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _WifiWidgetState();
}

class _WifiWidgetState extends State<WifiWidget> {
  final double _panelHeightOpen = 380.0;
  final double _panelHeightClosed = 95.0;

  final List<String> networks = [
    "Minha Casa",
    "Vizinho 01",
    "Vizinho 02",
    "Vizinho 03",
    "Vizinho 04"
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Theme.of(context).primaryColor,
        child: SafeArea(
            child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 20),
                    child: SvgPicture.asset('assets/icons/wifi_header.svg'),
                  ),
                  RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                            text: S.of(context).wifi_headline,
                            style: Theme.of(context).textTheme.title),
                        TextSpan(
                            text: S.of(context).wifi_subhead,
                            style: Theme.of(context).textTheme.headline)
                      ],
                    ),
                  )
                ],
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                child: SlidingUpPanel(
                  renderPanelSheet: false,
                  maxHeight: _panelHeightOpen,
                  minHeight: _panelHeightClosed,
                  panel: Container(
                      color: Theme.of(context).primaryColor,
                      child: Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10.0),
                                  topRight: Radius.circular(10.0))),
                          child: Stack(
                            overflow: Overflow.visible,
                            alignment: Alignment.topCenter,
                            children: <Widget>[
                              Positioned(
                                top: -15,
                                width: 50,
                                height: 5,
                                child: Container(
                                  width: 50,
                                  height: 5,
                                  decoration: BoxDecoration(
                                      color: Colors.grey[300],
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(12.0))),
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Expanded(
                                    child: ListView.builder(
                                        itemCount: networks.length,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return ListTile(
                                            contentPadding:
                                                EdgeInsets.all(10.0),
                                            onTap: () =>
                                                Connection.showConnectionDialog(
                                                    context, networks[index]),
                                            leading: index == 0
                                                ? SvgPicture.asset(
                                                    'assets/icons/wifi_network_own.svg')
                                                : SvgPicture.asset(
                                                    'assets/icons/wifi_networks_others.svg'),
                                            title: Text(networks[index]),
                                            trailing: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                index == 0
                                                    ? SvgPicture.asset(
                                                        'assets/icons/wifi_level_own.svg')
                                                    : SvgPicture.asset(
                                                        'assets/icons/wifi_level_others.svg'),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: Text('57%'),
                                                )
                                              ],
                                            ),
                                          );
                                        }),
                                  )
                                ],
                              )
                            ],
                          ))),
                ))
          ],
        )));
  }
}
