import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:Vanecontrol/generated/i18n.dart';

class Connection {

   static void _showConnectionSuccefullDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            contentPadding: EdgeInsets.symmetric(horizontal: 20),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            content: ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 205.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Text(
                          S
                              .of(context)
                              .wifi_connection_succefull_dialog_title,
                          style: TextStyle(
                              fontFamily: 'HelveticaNeue_Roman',
                              fontSize: 16,
                              color: Color(0xFF4D4D4D)))),
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: SvgPicture.asset('assets/icons/success.svg'),
                  )
                ],
              ),
            ));
      },
    );
  }

  static void showConnectionDialog(BuildContext context, String networkName) {
    // flutter defined function
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
            contentPadding: EdgeInsets.symmetric(horizontal: 20),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
            content: ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 205.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 15),
                      child: Text(S.of(context).wifi_dialog_title,
                          style: TextStyle(
                              fontFamily: 'HelveticaNeue_Roman',
                              fontSize: 16,
                              color: Color(0xFF4D4D4D)))),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    child: Text(networkName,
                        style: TextStyle(
                            fontFamily: 'HelveticaNeue_Bold', fontSize: 20)),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: SizedBox(
                          width: double.infinity,
                          height: 45,
                          child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                _showConnectionSuccefullDialog(context);
                              },
                              color: Theme.of(context).primaryColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50.0)),
                              child: Text(
                                S.of(context).wifi_btn_connect.toUpperCase(),
                                style: TextStyle(color: Colors.white),
                              )))),
                  FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        S.of(context).wifi_btn_cancel,
                        style:
                        TextStyle(color: Theme.of(context).primaryColor),
                      ))
                ],
              ),
            ));
      },
    );
  }
}