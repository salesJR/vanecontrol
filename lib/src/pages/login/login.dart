import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';

import '../../widget/toggle_field.dart';
import 'package:Vanecontrol/src/pages/welcome/welcome.dart';

class LoginWidget extends StatefulWidget {
  LoginWidget({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  final _formKey = GlobalKey<FormState>();
  final _emailFieldController = TextEditingController();
  final _passwordFieldController = TextEditingController();

  @override
  void dispose() {
    _emailFieldController.dispose();
    _passwordFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    String _validateEmailField(String value) {
      if (value.isEmpty) {
        return 'Please enter some text';
      }

      Pattern pattern =
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regex = new RegExp(pattern);
      if (!regex.hasMatch(value)) {
        return 'Enter a valid Email';
      }
      return null;
    }

    return Material(
        child: Container(
            color: Theme.of(context).primaryColor,
            child: Padding(
                padding: EdgeInsets.symmetric(vertical: 100, horizontal: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    SvgPicture.asset('assets/icons/marca_vane.svg'),
                    Form(
                        key: _formKey,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                      keyboardType: TextInputType.emailAddress,
                                      controller: _emailFieldController,
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder(
                                              borderSide: BorderSide.none),
                                          fillColor: Colors.white,
                                          filled: true,
                                          hintText: 'Usuário',
                                          prefixIcon: IconButton(
                                              icon: SvgPicture.asset(
                                                  'assets/icons/user.svg'))),
                                      validator: _validateEmailField)),
                              ToggleFieldWidget(
                                  controller: _passwordFieldController),
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: FlatButton(
                                    child: Text(
                                      'Problemas com a senha?',
                                      textAlign: TextAlign.right,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontStyle: FontStyle.italic),
                                    ),
                                  )),
                              Padding(
                                padding: EdgeInsets.only(top: 50),
                                child: SizedBox(
                                    width: double.infinity,
                                    height: 65,
                                    child: Padding(
                                        padding: EdgeInsets.all(10),
                                        child: RaisedButton(
                                            onPressed: () {
                                              if (_formKey.currentState
                                                  .validate()) {
                                                Navigator.pushReplacement(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            WelcomeWidget()));
                                              }
                                            },
                                            color:
                                                Theme.of(context).accentColor,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        50.0)),
                                            child: Text(
                                              'ENTRAR',
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )))),
                              ),
                              Padding(
                                  padding: EdgeInsets.symmetric(vertical: 20),
                                  child: FlatButton(
                                    child: Text(
                                      'CRIAR UMA CONTA',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ))
                            ]))
                  ],
                ))));
  }
}
