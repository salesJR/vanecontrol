import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import 'package:barcode_scan/barcode_scan.dart';

import 'package:Vanecontrol/generated/i18n.dart';

import 'package:Vanecontrol/src/pages/wifi/selection.dart';

final platform = Platform.isAndroid;

class QRCodeBtnReading extends StatelessWidget {

  Future _scan(context) async {
    try {
      String barcode = await BarcodeScanner.scan();
      Navigator.pushReplacement(
          context,
          platform
              ? MaterialPageRoute(
                  builder: (BuildContext context) => WifiWidget())
              : CupertinoPageRoute(
                  builder: (BuildContext context) => WifiWidget()));
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
      } else {}
    } on FormatException {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {

    final _btnReadingText = Text(
      S.of(context).btn_start_reading_qrcode.toUpperCase(),
      style: TextStyle(
        color: Colors.white
      ),
    );

    final _borderRadius = BorderRadius.circular(50.0);

    return platform
        ? RaisedButton(
            onPressed: () {
              _scan(context);
            },
            color: Theme.of(context).accentColor,
            shape: RoundedRectangleBorder(
                borderRadius: _borderRadius),
            child: _btnReadingText
    )
        : CupertinoButton(
            onPressed: () {
              _scan(context);
            },
            color: CupertinoTheme.of(context).primaryContrastingColor,
            borderRadius: _borderRadius,
            child: _btnReadingText
          );
  }
}
