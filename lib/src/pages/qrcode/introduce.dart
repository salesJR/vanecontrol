import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:Vanecontrol/src/pages/qrcode/inner.dart';

class QRCodeIntroduceWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? Material(
            color: Theme.of(context).primaryColor, child: QRCodeInnerWidget())
        : CupertinoPageScaffold(
            backgroundColor: CupertinoTheme.of(context).primaryColor,
            child: QRCodeInnerWidget(),
          );
  }
}
