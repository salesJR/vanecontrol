import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:Vanecontrol/generated/i18n.dart';

import 'package:Vanecontrol/src/pages/qrcode/btn_reading.dart';

final _platform = Platform.isAndroid;

class QRCodeInnerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        children: <Widget>[
          Align(
              alignment: Alignment.center,
              child: Container(
                margin: const EdgeInsets.only(bottom: 100.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(bottom: 20.0),
                      child:
                          SvgPicture.asset('assets/icons/qrcode_introduce.svg'),
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                              text: S.of(context).qrcode_introduce_headline,
                              style: _platform
                                  ? Theme.of(context).textTheme.title
                                  : CupertinoTheme.of(context)
                                      .textTheme
                                      .navLargeTitleTextStyle),
                          TextSpan(
                              text: S.of(context).qrcode_introduce_subhead,
                              style: _platform
                                  ? Theme.of(context).textTheme.headline
                                  : CupertinoTheme.of(context)
                                      .textTheme
                                      .tabLabelTextStyle
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                margin: const EdgeInsets.all(50.0),
                alignment: Alignment.bottomCenter,
                child: SizedBox(
                    width: double.infinity,
                    height: 45,
                    child: QRCodeBtnReading())),
          )
        ],
      ),
    );
  }
}
