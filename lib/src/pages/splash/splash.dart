import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';

import 'package:Vanecontrol/src/pages/login/login.dart';

class SplashWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashWidgetState();
}

class _SplashWidgetState extends State<SplashWidget> {
  @override
  void initState() {
    super.initState();
    _startSplashScreenTimer();
  }

  _startSplashScreenTimer() async {
    final _duration = Duration(seconds: 2);
    return new Timer(_duration, _navigationToNextPage);
  }

  void _navigationToNextPage() {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => LoginWidget()));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return Material(
        child: Container(
          alignment: Alignment.center, 
          child: Text('Splash')
        )
      );
  }
}
