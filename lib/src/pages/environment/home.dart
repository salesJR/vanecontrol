import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

import 'package:Vanecontrol/generated/i18n.dart';

class EnvHomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        drawer: Drawer(),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          elevation: 0,
        ),
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Column(children: <Widget>[
                Container(
                    decoration:
                        BoxDecoration(color: Theme.of(context).primaryColor),
                    constraints: BoxConstraints(maxHeight: 230),
                    child: Stack(
                        alignment: Alignment.topCenter,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.topCenter,
                            child: SvgPicture.asset(
                                'assets/icons/welcome_background.svg'),
                          ),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: SvgPicture.asset(
                                      'assets/icons/devices.svg'),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(top: 15),
                                  child: Text(S.of(context).env_label,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontFamily: 'HelveticaNeue_Roman',
                                          fontSize: 12,
                                          color: Colors.white.withOpacity(.7))),
                                ),
                                SizedBox(
                                    width: double.infinity,
                                    height: 50,
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 0),
                                      child: Card(),
                                    ))
                              ]),
                        ])),
                Expanded(
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white),
                  ),
                )
              ])
            ],
          ),
        ));
  }
}
